#!/bin/bash
ELASTICSEARCH_REPO="/etc/yum.repos.d/aops_elascticsearch.repo"
IP=$1

function check_es_status() {
  visit_es_response=$(curl -s -XGET http://127.0.0.1:9200)
  if [[ "${visit_es_response}" =~ "You Know, for Search" ]]; then
    echo "[ERROR] The service is running, please close it manually and try again."
    exit 1
  fi
}

function create_es_repo() {
  echo "[INFO] Start to create ES official installation repo"

  if [ ! -f ${ELASTICSEARCH_REPO} ]; then
    touch ${ELASTICSEARCH_REPO}
  fi
  echo "[aops_elasticsearch]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md" >${ELASTICSEARCH_REPO}
  # create repo file failed
  if [ ! -f ${ELASTICSEARCH_REPO} ]; then
    echo "[ERROR] aops_elasticsearch.repo file creation failed!"
    echo "Please confirm whether you have permission!"
    exit 1
  fi

  echo "[INFO] Create ES repo success"
}

function download_install_es() {
  echo "[INFO] start to download and install elasticsearch"
  # check repo
  es_repo=$(yum repolist | grep "aops_elasticsearch")
  if [ -z "${es_repo}" ]; then
    echo "[ERROR] not found elasticsearch repo,please check config file in /etc/yum.repos.d"
    exit 1
  fi

  if yum install elasticsearch-7.14.0-1 -y; then
    echo "[INFO]download and install elasticsearch success"
  else
    echo "[ERROR] install elasticsearch failed"
    exit 1
  fi

}

function start_elasticsearch_service() {
  echo "[INFO] start to start elasticsearch service"
  sudo /bin/systemctl daemon-reload
  sudo /bin/systemctl enable elasticsearch.service
  sudo /bin/systemctl start elasticsearch.service
  for i in {1..12}; do
    visit_es_response=$(curl -s -XGET http://127.0.0.1:9200)
    if [[ "${visit_es_response}" =~ "You Know, for Search" ]]; then
      echo "[INFO] elasticsearch start success"
      exit 0
    fi
    sleep 5
  done

  echo "[ERROR] elasticsearch start failed,please check /var/log/elasticsearch/elasticsearch.log"
  exit 1
}

function download_install_mysql() {
  echo "[INFO] start to download and install mysql"

  if yum install mysql-server -y; then
    echo "[INFO] download and install mysql success"
  else
    echo "[ERROR] install mysql failed"
    echo "[ERROR] Please check the files under the /etc/yum.repos.d/ is config correct"
    exit 1
  fi
}

function start_mysql_service() {
  echo "[INFO] start to start mysql service"
  sudo systemctl start mysqld
  for i in {1..12}; do
    mysql_pid=$(pgrep mysqld)
    if [[ -n ${mysql_pid} ]]; then
      echo "[INFO] mysql start success"
      exit 0
    fi
    sleep 2
  done

  echo "[ERROR] mysql start failed,please check /var/log/mysql/mysql.log"
  exit 1
}

function download_install_redis() {
  echo "[INFO] start to download and install redis"

  if yum install redis -y; then
    echo "[INFO] download and install redis success"
  else
    echo "[ERROR] install redis failed"
    echo "[ERROR] Please check the files under the /etc/yum.repos.d/ is config correct"
    exit 1
  fi
}

function install_microservice(){
    echo "[INFO] start to download and install aops service"

    yum install aops-apollo aops-zeus aops-hermes aops-ceres -y

    if [ $? -ne 0 ]; then
        echo "[ERROR] An error occurred installing the aops-apollo aops-zeus aops-hermes aops-ceres"
        exit 1
    fi
    echo "[INFO] microservice installation is complete"
}

function install_zookeeper(){
    echo "[INFO] start to download and install zookeeper"
    yum install zookeeper -y

    if [ $? -ne 0 ]; then
        echo "[ERROR] install elasticsearch failed"
        exit 1
    fi
}

function set_microservice_config(){
    echo "[INFO] start setting the micros ervice configuration"
    # echo "[INFO] set mysql ip"
    # sed -i "/\[mysql\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/apollo.ini
    # sed -i "/\[mysql\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini

    echo "[INFO] set elasticsearch ip"
    sed -i "/\[elasticsearch\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/apollo.ini
    sed -i "/\[elasticsearch\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini

    echo "[INFO] set redis ip"
    sed -i "/\[redis\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/apollo.ini
    sed -i "/\[redis\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini
    
    echo "[INFO] set prometheus ip"
    sed -i "/\[prometheus\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini


    echo "[INFO] set apollo ip"
    sed -i "/\[apollo\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/apollo.ini
    sed -i "/\[apollo\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini

    echo "[INFO] set zeus ip"
    sed -i "/\[zeus\]/,/\[/ s/^ip=.*/ip=$IP/" /etc/aops/zeus.ini
    
    echo "[INFO] the microservice configuration items are configured"
}

function set_nginx_config(){
    echo "[INFO] start setting the nginx proxy pass configuration"
    sed -i -E "s/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/$IP/g" /etc/nginx/aops-nginx.conf
    echo "[INFO] nginx configuration is complete"
}

function set_elasticsearch_config(){
    echo "[INFO] start setting the elasticsearch configuration"
    sed -i -E "s/(network\.host:\s+)[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/\1$IP/" /etc/elasticsearch/elasticsearch.yml
    echo "[INFO] elasticsearch configuration is complete"
}

function set_redis_config(){
    echo "[INFO] start setting the redis configuration"
    sed -i -E "s/(^bind:\s+)([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/\1\2 $IP/" /etc/redis.conf
    echo "[INFO] redis configuration is complete"
}

function set_mysql_config(){
    echo "[INFO] start setting the redis configuration"
    echo "[mysqld]
bind-address=$IP" >/etc/my.cnf
    echo "[INFO] mysql configuration is complete"

}

function load_sql(){
    echo "[INFO] start create database"
    ehco "[INFO] "
    mysql  mysql < /opt/aops/database/zeus.sql
    mysql  mysql < /opt/aops/database/apollo.sql
    echo "[INFO] database initialization is complete"
}

function is_valid_ip() {
    valid_check=$(echo $IP|awk -F. '$1<=255&&$2<=255&&$3<=255&&$4<=255{print "yes"}')
    if echo $IP|grep -E "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$">/dev/null; then
        if [ ${valid_check:-no} == "yes" ]; then
            echo "[INFO] IP $IP available."
        else
            echo "[ERROR] IP $IP not available!"
            exit 1
        fi
    else
        echo "[ERROR] IP format error!"
        exit 1
    fi
}

function start_base_env(){
    echo "[INFO] start mysql"
    systemctl start mysqld
    systemctl status mysqld
    echo "[INFO] start redis"
    systemctl start redis
    systemctl status redis
    echo "[INFO] start elasticsearch"
    systemctl start elasticsearch
    systemctl status elasticsearch
    echo "[INFO] start zookeeper"
    systemctl start zookeeper
    systemctl status zookeeper
    echo "[INFO] the basic environment is started"
}

function start_micro_service(){
    echo "[INFO] start zeus"
    systemctl start aops-zeus
    systemctl status aops-zeus
    echo "[INFO] start apollo"
    systemctl start aops-apollo
    systemctl status aops-apollo
    echo "[INFO] start hermes"
    systemctl start aops-hermes
    systemctl status aops-hermes
    echo "[INFO] The microservices and clients are started"
}

function main(){
    check_es_status
    create_es_repo
    download_install_mysql
    download_install_es
    download_install_redis
    install_zookeeper
    install_microservice
    
    if [ "$IP" != "" ]; then
        is_valid_ip
        set_microservice_config
        set_nginx_config
        set_redis_config
        set_elasticsearch_config
        set_mysql_config
    fi
    start_base_env
    start_micro_service
}

main $1