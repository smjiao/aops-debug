#!/usr/bin/python3
# ******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN 'AS IS' BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# ******************************************************************************/
"""
Time:
Author:
Description: 
"""
import os
from abc import abstractmethod
import yaml
from typing import Any
from flask_apscheduler import APScheduler

from vulcanus.log.log import LOGGER


class BaseTimedManage():
    """
    Classes for Timed Management, it can add, delete, pause and view scheduled tasks.
    """
    timed_scheduler = APScheduler()
    timed_config = None
    timed_type = ["date", "cron", "interval"]

    def __init__(self, app, timed_config) -> None:
        self._init_app(app)
        self._timed_config = None
        if BaseTimedManage.timed_config is None:
            BaseTimedManage.timed_config = self.__timed_config(file_path=timed_config)

    def __timed_config(self, file_path: str) -> dict:
        """
        Parsing the configuration file information of a timed task

        Args:
            file_path(str): Path to the configuration file

        Returns:
            list: list of dict, each dict is a parsing results, e.g.:
                {
                    "cve_scan":{
                                'timed_name': 'cves scan',
                                'task': 'cve_scan',
                                'auto_start': true,
                                'meta': {},
                                'timed':{
                                    'day_of_week': '0-6',
                                    'hour': '2',
                                    'trigger': 'cron'
                                }

                            }
                }
        """
        if not os.path.exists(file_path):
            raise FileNotFoundError("File does not exist: %s" % file_path)

        # load yaml configuration file
        with open(file_path, "r", encoding="utf-8") as file_context:
            try:
                timed_config = yaml.safe_load(file_context.read())
            except yaml.YAMLError as error:
                LOGGER.error(
                    "The format of the yaml configuration file is wrong please check and try again:{0}".format(error))
                return dict()

        return {config.get("task"): config for config in timed_config}

    def _check_(self, task_id=None) -> bool:
        timed = self._timed_config.get("timed")
        trigger = timed.get("trigger", "cron")
        if trigger not in self.timed_type:
            LOGGER.error("Wrong trigger parameter for timed tasks.")
            return False

        if task_id is None:
            if "timed_name" not in self._timed_config:
                LOGGER.error("Create scheduled task is missing timed_name fields.")
                return False

            self._timed_config['timed']['id'] = self._timed_config["timed_name"]
        else:
            if self.get_timed(task_id):
                LOGGER.info("This task id already exists for a scheduled task.")
                return False
            self._timed_config['timed']['id'] = task_id

        if "day_of_week" not in timed or "hour" not in timed:
            LOGGER.error("Create scheduled task is missing required  fields.")
            return False

        return True

    @abstractmethod
    def _task(self):
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self._task()

    def _init_app(self, app):
        """
        Initialize APScheduler

        Args:
            app:flask.Application
        """
        self.timed_scheduler.init_app(app)

    def start(self):
        """
        Start running scheduled tasks
        """
        self.timed_scheduler.start()

    def _get_timed_config(self):
        if "meta" in self._timed_config:
            self._timed_config.pop("meta")

        self._timed_config["timed"]['func'] = self
        if not self._timed_config.get("enable", False):
            LOGGER.info(f"{self._timed_config['timed']['id']}, This task is configured to not start.")
            return None

        return self._timed_config["timed"]

    def _add_job(self, task_id):
        if not self._check_(task_id):
            return
        timed_config = self._get_timed_config()

        if timed_config:
            self.timed_scheduler.add_job(**timed_config)

    def add_job(self, timed, task_id=None, **kwargs):
        """
        Create a timed task.

        Args:
            func(str): Functions for scheduled task execution
            task_id(str): The name of the task
            trigger(str): Timed task start method, value "date" or "cron" or "interval"
            kwargs: Parameters needed to create a timed task
                If the trigger method is "date", the parameter format is:
                {
                    "date","2022-2-3 00:00:01"
                }

                If the trigger method is "interval", the parameter format is:
                    {
                        "weeks" or "days" or "hours" or "minutes" or "seconds": time interval
                    }

                If the trigger method is "cron", the parameter format is:
                    {
                        "weeks" or "days" or "hours" or "minutes" or "seconds": time
                    }

        """
        timed._add_job(task_id)

    def pause(self, task_id: str):
        """
        Pause a timed task for an id

        Args:
            task_id (str): timed task id;
        """
        self.timed_scheduler.pause_job(task_id)

    def resume(self, task_id: str):
        """
        Resume a timed task for an id

        Args:
            task_id (str): timed task id;
        """
        self.timed_scheduler.resume_job(task_id)

    def get_timed(self, task_id=None, all=False):
        """
        Get a timed task or all timed task

        Args:
            task_id: id of a scheduled task, id cannot be empty when get a single task
            all: Get all or single
        Return:
            list: List of all timed task
        """
        if all:
            return self.timed_scheduler.get_jobs()

        return self.timed_scheduler.get_job(task_id)

    def delete_timed(self, task_id: str):
        """
        Delete the scheduled task of the corresponding id

        Args:
            task_id (str): delete timed task id;
        """
        self.timed_scheduler.delete_job(task_id)
